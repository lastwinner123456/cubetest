using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField] float spawnTime;
    [SerializeField] float speed;
    [SerializeField] float destroyDistance;
    [SerializeField] GameObject spawnPosition;
    [SerializeField] GameObject spawnPrefab;
    float StartTime;

    void Start()
    {
        StartTime =spawnTime;
        Spawning();
    }

    void Update()
    {

        StartTime-= Time.deltaTime;
        if (StartTime <= 0)
        {
            Spawning();
        }
    }
    void Spawning()
    {
        GameObject cube = Instantiate(this.spawnPrefab, spawnPosition.transform.position, Quaternion.identity);
        cube.GetComponent<Cube>().speed = speed;
        cube.GetComponent<Cube>().destroyDistance = destroyDistance;
        StartTime = spawnTime;
    }
}
