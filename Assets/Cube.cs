using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public float speed;
    public float destroyDistance;
    GameObject spawnPosition;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        spawnPosition = GameObject.FindGameObjectWithTag("Spawn");
        rb.velocity = Vector3.forward * speed * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
       
        float distance = Vector3.Distance(transform.position, spawnPosition.transform.position);
        if (distance >= destroyDistance)
        {
            Destroy(gameObject);
        }
    }
}
